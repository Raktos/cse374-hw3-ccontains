// Jason Ho
// CSE 374 HW3
// 30 January 2016

// gasp
// This program reads a files and prints our line
// that contain a match to a pattern string

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAX_LINE_LENGTH 500
#define MAX_PATTERN_LENGTH 100
#define MAX_FILE_NAME_LENGTH 50

void processFile(char *fileName, char *pattern, int ignoreCase, int lineNums);
int containsPattern(char *line, char *pattern);
int containsPattern(char *line, char *pattern);
int containsPatternCaseInsensitive(char *line, char *pattern);
void strToLower(char *string);


int main(int argc, char **argv) {
  if (argc < 2) {
    printf("usage: gasp [options] STRING FILE...\n");
  } else {
    // variables to store the arguments in after processing
    int ignoreCase = 0;
    int lineNums = 0;
    char pattern[MAX_PATTERN_LENGTH];
    pattern[0] = '\0';
    char files[argc][MAX_FILE_NAME_LENGTH];
    int numFiles = 0;

    // does the arguent processing
    for (int i = 1; i < argc; i++) {
      if (strcmp("-i", argv[i]) == 0) {
        ignoreCase = 1;
      } else if (strcmp("-n", argv[i]) == 0) {
        lineNums = 1;
      } else if (pattern[0] ==  '\0') {
        strncpy(pattern, argv[i], MAX_PATTERN_LENGTH);
      } else {
        strncpy(files[numFiles], argv[i], MAX_FILE_NAME_LENGTH);
        numFiles++;
      }
    }

    // Do the actual processing
    for (int i = 0; i < numFiles; i++) {
      processFile(files[i], pattern, ignoreCase, lineNums);
    }
  }

  return 0;
}

// processes a single file
// takes a fileName string, a pattern string
// takes checks for ignoring case and printing line numbers
// simply prints the file cannot be found with the name to stderr
// if the file cannot be opened for some reason
// returns void
void processFile(char *fileName, char* pattern, int ignoreCase, int lineNums) {
  FILE *file = fopen(fileName, "r");

  if (file != NULL) {
    // line will store the line read in from the file
    // curLine keeps track of which line we're on
    char line[MAX_LINE_LENGTH];
    int curLine = 1;

    // loop over the file
    while (fgets(line, MAX_LINE_LENGTH, file)) {
      // check if we can pass this line based on ignoreCase status
      int linePass = 0;
      if (ignoreCase == 0) {
        linePass = containsPattern(line, pattern);
      } else {
        linePass = containsPatternCaseInsensitive(line, pattern);
      }

      // If the line passed print based on lineNums status
      if (linePass != 0) {
        if (lineNums != 0) {
          printf("%s : %i : %s", fileName, curLine, line);
        } else {
          printf("%s : %s", fileName, line);
        }
      }
      curLine++;
    }
    fclose(file);
  } else {
    fprintf(stderr, "gasp: file %s not found.\n", fileName);
  }
}

// checks if a line string contains a pattern string
// returns 1 if the line contains the pattern
// returns 0 if the line does NOT contain the pattern
int containsPattern(char *line, char *pattern) {
  if (strstr(line, pattern)) {
    return 1;
  }
  return 0;
}

// checks if a line string contains a pattern string, case insensitive
// retuns the result of containsPattern after converting
// the line and pattern to lowercase
int containsPatternCaseInsensitive(char *line, char *pattern) {
  strToLower(line);
  strToLower(pattern);
  return containsPattern(line, pattern);
}

// converts a given string to lowercase
// returns void
void strToLower(char *string) {
  for (int i = 0; string[i]; i++) {
    string[i] = tolower(string[i]);
  }
}
